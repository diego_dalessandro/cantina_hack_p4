require 'rest-client'
require 'json'

require_relative 'screen_manager/screen_manager'
require_relative 'views/login'
require_relative 'views/main_menu'
require_relative 'views/clients_menu'
require_relative 'views/new_client'
require_relative 'views/import_client'
require_relative 'views/find_client'
require_relative 'views/index_clients'

require_relative 'views/products_menu'
require_relative 'views/new_product'
require_relative 'views/find_product'
require_relative 'views/index_products'

require_relative 'views/invoices_menu'
require_relative 'views/new_invoice'
require_relative 'views/find_invoice'
require_relative 'views/index_invoices'

class App
  @@authorization = nil
  @@cantina_server = 'localhost:3000/'
  @@users_server = 'localhost:4000/'

  @@email = nil
  @@password = nil

  def self.authorization
    @@Authorization
  end

  def self.authorization=(authorization = nil)
    @@Authorization = authorization
  end

  def self.cantina_server
    @@cantina_server
  end

  def self.users_server
    @@users_server
  end

  def self.login_data
    {email: @@email, password: @@password}
  end
  
  def self.login_data= (login_data = {})
    @@email = login_data[:email] || nil
    @@password = login_data[:password] || nil
  end

    
  
  
  def self.title(subtitle = '')
    puts "CANTINA HACK - Chucherias"
    puts
    puts subtitle.capitalize
    puts
  end
end

ScreenManager.start(Login.caller)