class Login < Screen
  @@end_point = 'auth/login'

  @@exit_menu = Menu.new(options: ['continuar', 'salir'])
  @@exit_menu.add_action('salir') do
    ScreenManager.exit
  end
  @@exit_menu.add_action('continuar') do
    self.request_data
    self.send_data
  end




  def self.display
    App.title "ingreso al sistema"
    
    puts "¿Desea salir del sistema?  "
    @@exit_menu.display

  end

  def self.request_data
    # solicitamos los datos para el login al usuario
    puts "por favor ingrese los datos solicitados"
    print "email: "
    email = gets.chomp
    print "password: "
    password = gets.chomp
    puts

    App.login_data = {email: email, password: password}
  end

  def self.send_data
    # enviamos la data suministrada y esperamos respuesta del servidor
    response = RestClient.post "#{App.cantina_server}#{@@end_point}", {authorized_user: App.login_data}
    response = JSON.parse(response.body)

    # manejamos la respuesta del servidor
    if response['api_key'].nil?
      puts response['message']
      puts "Presiona cualquier tecla para intentarlo nuevamente"
      App.login_data = {}
      self.get_keypressed
    else
      App.authorization = response['api_key']
      puts "Has ingresado con éxito"
      self.next_screen = MainMenu.caller #(self.caller)
    end
  end

end
