class InvoicesMenu < Screen


  @@menu = Menu.new(options: ['nuevo', 'buscar por id', 'listar todos', 'regresar'])
  @@menu.add_action('nuevo') {self.next_screen = NewInvoice.caller}
  @@menu.add_action('buscar por id') {self.next_screen = FindInvoice.caller}
  @@menu.add_action('listar todos') {self.next_screen = IndexInvoices.caller}
  @@menu.add_action('regresar') {self.next_screen = MainMenu.caller}

  def self.display
    # mostramos el menu principal
    App.title 'ventas'
    
    @@menu.display
  end
end