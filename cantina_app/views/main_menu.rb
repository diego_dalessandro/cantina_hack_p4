class MainMenu < Screen


  @@main_menu = Menu.new(options: ['clientes', 'productos', 'ventas', 'cerrar sesión'])
  @@main_menu.add_action('clientes') {self.next_screen = ClientsMenu.caller}
  @@main_menu.add_action('productos') {self.next_screen = ProductsMenu.caller}
  @@main_menu.add_action('ventas') {self.next_screen = InvoicesMenu.caller}
  @@main_menu.add_action('cerrar sesión') do 
    self.next_screen = Login.caller
    App.authorization = nil
  end

  def self.display
    # mostramos el menu principal
    App.title 'menu principal'
    
    @@main_menu.display
  end
end