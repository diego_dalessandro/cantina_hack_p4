class IndexClients < Screen
  @@end_point = 'clients'
  @@method = 'get'
  @@clients = nil
  

  def self.display
    # mostramos todos los clientes registrados
    App.title 'listado de clientes'
    self.join_clients_users
    self.get_keypressed
    self.next_screen = ClientsMenu.caller
  end

  def self.get_clients
    # solicitamos el index de clients al servidor
    response = RestClient::Request.execute(
      method: @@method, 
      url: "#{App.cantina_server}#{@@end_point}",
      headers: {Authorization: App.authorization, send: 'true'}
    )

    json_response = JSON.parse(response.body)
    clients = json_response['clients']

    clients
  end

  def self.get_users
    # enviamos la data de login para obtener la api_key
    response = RestClient.post "#{App.users_server}auth/login", {user: App.login_data}
    json_response = JSON.parse(response.body)
    authorization = json_response['api_key']

    # solicitamos el index de users al servidor de clients_api
    response = RestClient::Request.execute(
      method: 'get', 
      url: "#{App.users_server}users",
      headers: {Authorization: authorization, send: 'true'}
    )
    json_response = JSON.parse(response.body)
    users = json_response['users']

    users
  end

  def self.join_clients_users
    clients = self.get_clients
    users = self.get_users
    clients.each do |client|
      user = (users.select {|user| user['id'] == client['user_id']})[0]
      
      client['full_name'] = user['full_name']
      client['email'] = user['email']
      client['dni'] = user['dni']
      client['phones'] = user['phones'] unless user['phones'].nil?
    end

    self.print_clients(clients)
  end

  def self.print_clients(clients)
    clients.each do |client|
      puts "id: #{client['id']}"
      puts "name: #{client['full_name']}"
      puts "email: #{client['email']}"
      puts "balance: #{client['balance']}"
      puts "phone: #{client['phones'][0]['number']}" unless client['phones'].empty?
      puts
    end
  end

  
end