class ProductsMenu < Screen

  @@clients_menu = Menu.new(options: ['nuevo', 'buscar por id', 'listar todos', 'regresar'])
  @@clients_menu.add_action('nuevo') {self.next_screen = NewProduct.caller}
  @@clients_menu.add_action('buscar por id') {self.next_screen = FindProduct.caller}
  @@clients_menu.add_action('listar todos') {self.next_screen = IndexProducts.caller}
  @@clients_menu.add_action('regresar') {self.next_screen = MainMenu.caller}

  def self.display
    App.title 'productos'
    @@clients_menu.display
  end
  
end