class NewInvoice < Screen

  def self.display
    App.title 'Nueva venta'
    self.alert("Hemos llegado a la vista de registrar nueva venta")
    self.next_screen = InvoicesMenu.caller
  end
  
end