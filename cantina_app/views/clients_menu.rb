class ClientsMenu < Screen


  @@clients_menu = Menu.new(options: ['nuevo', 'importar', 'buscar por id', 'listar todos', 'regresar'])
  @@clients_menu.add_action('nuevo') {self.next_screen = NewClient.caller}
  @@clients_menu.add_action('importar') {self.next_screen = ImportClient.caller}
  @@clients_menu.add_action('buscar por id') {self.next_screen = FindClient.caller}
  @@clients_menu.add_action('listar todos') {self.next_screen = IndexClients.caller}
  @@clients_menu.add_action('regresar') {self.next_screen = MainMenu.caller}

  def self.display
    # mostramos el menu de clientes
    App.title 'clientes'
    @@clients_menu.display
  end
end