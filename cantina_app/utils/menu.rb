require 'colorize'

class Menu

  attr_reader :selection_index
  attr_writer 
  attr_accessor :text, :options
  attr_accessor :default_highlight, :current_highlight, :on_escape_select
  attr_accessor :capitalize, :separator, :vertical
  attr_accessor :padding,  :upper_margin, :lower_margin
  attr_accessor :highlight_color, :highlight_background
  
  
  def initialize(parameters = {})
    # Options initialization
    @bread_crumb = parameters[:bread_crumb] || ""
    @user_name = parameters[:bread_crumb] || ""
    @text = parameters[:text] || nil
    @options = parameters[:options] || []
    @actions = []

    @default_highlight = parameters[:default_highlight] || 0
    @on_escape_select = parameters[:on_escape_select] || @options.length - 1

    @capitalize = parameters[:capitalize] || true
    @separator = parameters[:separator] || ""
    @vertical = parameters[:vertical] || false

    @padding = parameters[:padding] || 1

    @highlight_color = parameters[:highlight_color] || :black
    @highlight_background = parameters[:highlight_background] || :white

    # Reset dinamic values
    reset
  end
  
  def reset
    @current_highlight = @default_highlight
    @selection_index = nil
    @exit = false
    self
  end

  def selection
    @options[@selection_index] unless @selection_index.nil?
  end

  def exit
    @exit = true
  end

  def add_action(option, &action)
    unless action.nil?
      option_index = @options.index(option)
      puts option_index
      @actions[option_index] = action
    end
  end

  def alert(text = "")
    puts text
    puts "Presione cualquier tecla para continuar"
    get_keypressed
  end

  def display
    loop do
      # Limpiamos la pantalla
      clear

      # Imprimimos el texto previo al menu
      print_text
      
      # Imprimimos las opciones
      print_options
      
      # Manejamos la entrada del usuario
      handle_input

      # Ejecutamos la accion designada
      execute_action unless selection_index.nil?
      
      break if @exit
    end
  end

  private 
  

  def clear
    system("clear")
    system("clear")
  end

  def print_text
    unless @text.nil?
      print @text 
      br 2
    end
  end

  def print_options
    @options.each_with_index do |option, index|
      option = option.capitalize if @capitalize
      option = option.center(option.length + 2*@padding)
      option = option.colorize(:background => @highlight_background).colorize(@highlight_color) if index == @current_highlight
      
      print option
      @vertical ? br : (print @separator)
    end
    @vertical ? (br) : (br 2)
  end

  def handle_input
    key_pressed = get_keypressed
    case key_pressed
    when 97, 119 then highlight_previous                # a, w
    when 100, 115 then highlight_next                   # d, s
    when 13 then @selection_index = @current_highlight  # Enter
    when 27 then @selection_index = @on_escape_select   # Escape 
    else 
    end
  end
  
  def execute_action
    if @actions[selection_index].nil?
      alert("Action not created")
    else
      @actions[selection_index].call
    end
    unselect
  end

  def highlight_previous
    @current_highlight -= 1 if @current_highlight > 0
  end

  def highlight_next
    @current_highlight += 1 if @current_highlight < @options.length - 1
  end

  def unselect
    @selection_index = nil
  end

  def br(new_lines = 1)
    new_lines.times {puts}
  end
  
  def get_keypressed
    system("stty raw -echo")
    t = STDIN.getc
    system("stty -raw echo")
    return t.downcase.ord
  end
end


=begin
COMO USAR

el menu esta comprendido por 3 elementos
text: el texto que aparece previo al menu, por defecto nil
options: arreglo de strings que contiene las opciones a mostrar, por defecto esta vacio
actions: arreglo de procs, cada proc debe ser la accion a ejecutarse cuando una opcion ha sido elegida, por defecto esta vacio

ademas contiene una serie de opciones
default_highlight: indica cual es la opcion que debe estar resaltada por defecto, por defecto es la opcion 0
on_escape_select: indica que accion debe ser seleccionada cuando se presiona la tecla escape, por defecto la ultima
capitalize: indica si, al mostrar las opciones, debe capitalizarlas, por defecto true
separator: indica que string debe colocarse entre una opcion y la otra, por defecto "". en el modo vertical no tiene efecto
vertical: indica si el menu debe mostrarse de forma vertical (una opcion en cada linea) u horizontal, por defecto false
padding: indica el espaciado que debe colocarse a ambos lados de cada opcion, por defecto 1
highlight_color: indica el color de fuente de la opcion resaltada, por defecto black. Debe ser una opcion valida del colorize
highlight_background: indica el color de fondo de la opcion resaltada, por defecto white. Debe ser una opcion valida del colorize

Ejemplo 1

menu_1 = Menu.new() # crea un menu con las opciones por defecto
menu_1.options = ["opcion 1", "opcion 2", "salir"] # agregamos las opciones
menu_1.add_action("opcion 1") { menu_1.alert("Has seleccionado #{menu_1.selection}") } 
  # con el metodo add_action(option) le damos un bloque con la accion a realizar cuando la opcion que pasamos por parametro sea elegida
  # el metodo .alert(text) sirve para mostrar un mensaje luego del menu. A este mensaje se le suma el texto "Presione cualquier tecla para continuar" y, al presionar una tecla, el codigo continua
menu_1.add_action("opcion 2") begin 
  Menu.new(options: ["opcion 3", "regresar"]) 
    # creamos la accion a generar cuando la opcion 2 sea seleccionada, la cual es crear un nuevo menu con las opciones "opcion 3" y "regresar"
end 
menu_1.add_action("salir") {menu_1.exit} 
  # generamos la accion para la opcion salir. con .exit indicamos que el menu termina y, en este caso, termina el programa

menu_1.display 
  # mostramos el menu. Este metodo tiene un loop interno que se ejecuta indefinidamente hasta que se ejecute el metodo Menu.exit dontro de el






=end