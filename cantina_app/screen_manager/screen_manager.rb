require_relative 'screen'



class ScreenManager
  @@exit = false


  def self.actual_screen
    @@actual_screen
  end

  def self.actual_screen= (new_screen_proc)
    @@actual_screen = new_screen_proc
  end

  def self.call_screen
    self.actual_screen.call
  end

  def self.exit?
    @@exit
  end

  def self.exit
    @@exit = true
  end



  
  def self.start(initial_screen)
    # Inicializamos la pantalla inicial
    self.actual_screen = initial_screen
    loop do
      # Borramos la pantalla
      self.clear

      # Llamamos a la pantalla actual
      self.actual_screen = self.call_screen

      # Terminamos el ciclo en caso de que no se indique una siguiente pantalla
      break if exit?
    end
    self.clear
  end

  def self.clear
    system("clear")
    system("clear")
  end
  
end


# Documentacion

# con hacer 
#   ScreenManager.start (MyScreen.caller) 
# inicias la aplicacion, donde MyScreen.caller es la clase, que hereda de Screen, que representa la vista inicial