require_relative 'menu_module'

class Screen
  include MenuModule
  
  def self.caller #(previous_screen = nil)
    #self.previous_screen = previous_screen unless previous_screen.nil?
    Proc.new do 
      self.next_screen = self.caller
      self.display
      self.next_screen
    end
  end

  def self.next_screen
    @@next_screen
  end
  
  def self.next_screen= (next_screen)
    @@next_screen = next_screen
  end

  # def self.previous_screen= (previous_screen)
  #   @@previous_screen = previous_screen
  # end

  # def self.previous_screen
  #   @@previous_screen
  # end

  # def self.return
  #   self.next_screen = self.previous_screen
  # end
  
  
  def self.display
  end

  def self.alert(message = "")
    p message
    puts
    self.get_keypressed
  end
  
  
  private

  def self.br(new_lines = 1)
    new_lines.times {puts}
  end

  def self.get_keypressed
    # Retorna el codigo ASCII de la proxima tecla que presione el usuario. 
    # En caso de que sea una letra, retorna el codigo de la version minuscula
    system("stty raw -echo")
    t = STDIN.getc
    system("stty -raw echo")
    return t.downcase.ord
  end

end

=begin
DOCUMENTACION

La clase Screen representa una "pantalla" o vista
Para hacer una vista, se debe crear una clase y hacer que herede de Screan
El contenido que debe mostrar la vista se coloca en el metodo self.display de la clase correspondiente
Ejemplo
Queremos crear una vista o pantalla llamada "Vista de bienvenida"
Hacemos una clase que hereda de Screen
  "class WelcomeScreen < Screen"
Y colocamos el contenido de la vista que queremos generar en el metodo self.display
def self.display
  puts "Bienvenido a mi app"
end

Si queremos redirigir a otra vista, digamos "Quienes somos", dentro del metodo self.display hacemos el siguiente comando
  self.next_screen = AboutUsScreen.caller (self.caller)
dentro del metodo self.display

El argumento "(self.caller)" indica que esta nueva vista "AboutUsScreen" guardara como vista anterior a la vista actual "WelcomeScreen"



Ejemplo
Creamos los siguientes archivos 
  my_app.rb
  welcome_screen.rb
  about_us_screen.rb
  exit_screen.rb

Copiamos los siguientes archivos
  app.rb
  screen.rb

Colocamos lo siguiente en cada uno de los archivos, como se indique

my_app.rb

  require_relative 'app'

  require_relative 'welcome_screen'
  require_relative 'about_us_screen'
  require_relative 'exit_screen'

  App.start(WelcomeScreen.me)


welcome_screen.rb

  class WelcomeScreen < Screen
    def self.display
      # Aqui comienza lo que la vista debe mostrar
      puts "WELCOME SCREEN"
      puts
      puts "Bienvenido a mi app, por favor dime tu nombre"
      name = gets.chomp
      puts "Gracias por visitarnos #{name}, presiona cualquier tecla para continuar"
      self.get_keypressed

      # Aqui indicamos que se debe ir a la siguiente vista al mismo tiempo que se indica cual sera
      self.next_screen = AboutUsScreen.me (self.me)
    end
  end


about_us_screen.rb

  class AboutUsScreen < Screen
    def self.display
      # Aqui comienza lo que la vista debe mostrar
      puts "ABOUT US SCREEN"
      puts
      puts "This is SPARTAAAAA"
      puts "Presiona esc para ir a la vista anterior, cualquier otra tecla para terminar"
      key_pressed = self.get_keypressed
      if key_pressed == 27
        self.next_screen = self.previous_screen
      else
        self.next_screen = ExitScreen.me (self.me) # De esta forma aqui indicamos que el programa valla a una tercera vista llamada "ExitScreen"
      end
    end
  end


exit_screen.rb

  class ExitScreen < Screen
    def self.display
      # Aqui comienza lo que la vista debe mostrar
      puts "EXIT SCREEN"
      puts
      puts "¿Seguro que quieres salir?"
      puts "Presiona x para salir, cualquier otra cosa te regresara a la vista anterior"
      key_pressed = self.get_keypressed
      if key_pressed == 120
        self.next_screen = nil # De esta forma aqui indicamos que no hay siguiente vista, y por lo tanto el programa terminara
      else
        self.next_screen = self.previous_screen
      end
    end
  end
  
Corremos todo haciendo ruby my_app.rb


=end
