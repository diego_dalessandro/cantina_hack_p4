require 'rest-client'
require 'json'
require_relative 'view'
require_relative '../views/login'

class App
  @@current_view = nil
  @@exit = false

  @@authorization = nil
  @@cantina_server = "localhost:3000/"
  @@clients_server = "localhost:4000/"

  def authorization= (authorization)
    @@authorization = authorization
  end

  def authorization
    @@authorization
  end

  def cantina_server
    @@cantina_server
  end

  def clients_server
    @@clients_server
  end

  def self.current_view
    @@current_view.call
  end

  def self.next= (next_view)
    @@current_view = next_view
  end

  def self.exit
    @@exit = true
  end

  def self.exit?
    @@exit
  end

  def self.start (intial_view)
    self.current_view = initial_view
    loop do
      self.clear

      self.current_view.call

      break if self.exit
    end
    self.clear
  end

  def self.clear
    system('clear')
    system('clear')
  end

end

puts App.start (Login.caller)