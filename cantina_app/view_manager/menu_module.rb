
module MenuModule
  require 'colorize'
  class Menu

    attr_reader :selection_index
    attr_writer 
    attr_accessor :options
    attr_accessor :default_highlight, :current_highlight, :on_escape_select
    attr_accessor :capitalize, :separator, :vertical
    attr_accessor :padding, :highlight_color, :highlight_background
    
    
    def initialize(parameters = {})
      # Options initialization
      @options = parameters[:options] || []
      @actions = {}

      @default_highlight = parameters[:default_highlight] || 0
      @on_escape_select = parameters[:on_escape_select] || @options.length - 1

      @capitalize = parameters[:capitalize] || true
      @separator = parameters[:separator] || ""
      @vertical = parameters[:vertical] || false

      @padding = parameters[:padding] || 1
      @highlight_color = parameters[:highlight_color] || :black
      @highlight_background = parameters[:highlight_background] || :white

      # Reset dinamic values
      reset
    end
    
    def reset
      @current_highlight = @default_highlight
      unselect
      self
    end

    def selection
      @options[@selection_index] unless @selection_index.nil?
    end

    def not_ready?
      @selection_index.nil?
    end

    def add_action(option, &action)
      @actions[option] = action unless action.nil?
    end

    def alert (message = "")
      puts "#{message}, presiona cualquier tecla para continuar"
      get_keypressed
    end
    def display
      # Imprimimos las opciones
      print_options
      
      # Manejamos la entrada del usuario
      handle_input

      # Ejecutamos la accion designada
      execute_action unless not_ready?
      
    end

    private 

    def print_options
      @options.each_with_index do |option, index|
        option = option.capitalize if @capitalize
        option = option.center(option.length + 2*@padding)
        option = option.colorize(:background => @highlight_background).colorize(@highlight_color) if index == @current_highlight
        
        print option
        @vertical ? (puts "") : (print @separator)
      end
      @vertical ? (puts "") : (puts ""; puts"")
    end

    def handle_input
      case get_keypressed
      when 97, 119 then highlight_previous                # a, w
      when 100, 115 then highlight_next                   # d, s
      when 13 then @selection_index = @current_highlight  # Enter
      when 27 then @selection_index = @on_escape_select   # Escape 
      else 
      end
    end
    
    def execute_action
      if @actions[selection].nil?
        alert("Action not created")
      else
        @actions[selection].call
      end
      unselect
    end

    def highlight_previous
      @current_highlight -= 1 if @current_highlight > 0
    end

    def highlight_next
      @current_highlight += 1 if @current_highlight < @options.length - 1
    end

    def unselect
      @selection_index = nil
    end

    def get_keypressed
      # Retorna el codigo ASCII de la proxima tecla que presione el usuario. 
      # En caso de que sea una letra, retorna el codigo de la version minuscula
      system("stty raw -echo")
      t = STDIN.getc
      system("stty -raw echo")
      return t.downcase.ord
    end

  end

end