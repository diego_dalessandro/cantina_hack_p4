require_relative 'menu_module'

class View
  include MenuModule
  
  def self.start

  end

  def self.display

  end

  def self.reset
    self.start
    self.caller
  end

  def self.caller
    return Proc.new {self.display}
  end

  def self.alert (message = "")
    p message
    puts
    self.get_keypressed
  end

  def self.get_keypressed
    # Retorna el codigo ASCII de la proxima tecla que presione el usuario. 
    # En caso de que sea una letra, retorna el codigo de la version minuscula
    system("stty raw -echo")
    t = STDIN.getc
    system("stty -raw echo")
    return t.downcase.ord
  end

end